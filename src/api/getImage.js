import no_poster from '../no_poster.jpg';

export default function getImage(path) {
  let posterImg = '';
  if (path === null) {
    posterImg = no_poster;
  } else {
    posterImg = `https://image.tmdb.org/t/p/original${path}`;
  }

  return posterImg;
};