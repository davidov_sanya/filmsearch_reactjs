import React from 'react';
import { connect } from 'react-redux'

import './App.css';
import Header from './components/Header/Header';
import Body from './components/Body/Body';
import Footer from './components/Footer';
import { getPopular } from './redux/actions/search';
import GoUp from './components/GoUp';


class App extends React.Component {

  componentDidMount(){
    this.props.getPopular();
  }

  render(){
    return (
      <div className="app">
        <GoUp/>
        <Header/>
        <Body/>
        <Footer/>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch){
  return {
    getPopular: () => dispatch(getPopular())
  }
}

export default connect(null, mapDispatchToProps)(App);
