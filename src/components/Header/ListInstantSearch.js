import React from 'react';
import { NavLink } from 'react-router-dom';
import '../../styles/list_instant_search.css';
import { DropdownItem } from 'reactstrap';
import getImage from '../../api/getImage';

class ListInstantSearch extends React.Component {

  onClick = (id) => () => {
    this.props.onClick(id);
    this.props.onToggle();
  };

  render(){
    const {
      list,
      header,
      imagePath,
      namePath,
      originalNamePath,
      nameContent
    } = this.props;


    return (
      <div>
        <DropdownItem header>{`${header}`}</DropdownItem>

        {list.map((item, i) => {
          if (i > 4) {
            return null;
          }

          const key = `${nameContent}_${i}`;
          return (
            <NavLink
              to={`/${nameContent}/${item.id}`}
              key={key}
              className="list_instant_search"
              onClick={this.onClick(item.id)}
            >

                <img src={getImage(item[imagePath])} width="24px" alt={key}/>
                <div>
                  <h1>{item[namePath]}</h1>
                  {item[originalNamePath] &&
                  <h2 className="original-name">{item[originalNamePath]}</h2>
                  }
                </div>

                {item.vote_average != null &&
                <h3 className="rating">
                  {( item.vote_average === 0 ) ? ` —` : ` ${item.vote_average}`}
                </h3>
                }

             </NavLink>
          )
        })}

      </div>
    )
  }
}

export default ListInstantSearch;
