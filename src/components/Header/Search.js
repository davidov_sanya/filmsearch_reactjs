import React from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import { Dropdown, DropdownMenu, DropdownToggle, Input } from 'reactstrap';
import InstantSearch from './InstantSearch';
import { getFilmList, getPersonList, getTVShowList, onSearchResults } from '../../redux/actions/search';
import '../../styles/search.css';

class Search extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      isShowSearchList: true,
      isOpen: false,
      searchValue: '',
    }
  }

  onChange = (event) => {
    let searchValue = event.target.value;
    this.setState({
      isOpen: true,
      isShowSearchList: true,
      searchValue
    });

    this.props.getFilmList(searchValue);
    this.props.getTVShowList(searchValue);
    this.props.getPersonList(searchValue);
  };


  onToggle = () => {
    this.setState({
      isShowSearchList: false
    });
  };

  onKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.onToggleResults();
      this.props.history.push('/SearchResults');
    }
  };

  onToggleResults = () => {
    this.setState({
      isShowSearchList: false
    });
    this.props.onSearchResults();
  };

  render(){
    return (
      <Dropdown
        className="search"
        isOpen={this.state.isOpen && this.state.isShowSearchList}
        toggle={() => {
          this.setState({
            isOpen: !this.state.isOpen
          })
        }}
        onFocus={() => {
          this.setState({
            isOpen: true,
            isShowSearchList: true
          })
        }}
        onClick={() => {
          this.setState({
            isOpen: true
          })
        }}
      >

        <DropdownToggle tag="div">
          <Input
            className="text-search"
            value={this.state.value}
            onChange={this.onChange}
            onKeyPress={this.onKeyPress}
          />
          <Link to="/SearchResults">
            <button
              className="search-button fas fa-search"
              onClick={this.onToggleResults}
            />
          </Link>
        </DropdownToggle>

        <DropdownMenu
          className="search-dropdown-menu"
          flip={false}
          positionFixed={true}
        >
          <InstantSearch
            onToggle={this.onToggle}
          />
        </DropdownMenu>

      </Dropdown>
    )
  }
}

function mapDispatchToProps(dispatch){
  return {
    onSearchResults: value => dispatch(onSearchResults(value)),
    getFilmList: value => dispatch(getFilmList(value)),
    getTVShowList: value => dispatch(getTVShowList(value)),
    getPersonList: value => dispatch(getPersonList(value))
  }
}

export default withRouter(connect(null, mapDispatchToProps)(Search));
