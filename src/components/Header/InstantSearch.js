import React from 'react'
import { connect } from 'react-redux';
import '../../styles/instant_search.css'
import ListInstantSearch from './ListInstantSearch';
import { getFilm, getPerson, getTVShow } from '../../redux/actions/search';

class InstantSearch extends React.Component {

  render(){
    const {
      instantFilmList,
      instantTVShowList,
      instantPersonList
    } = this.props;
    return (
      <>

        {instantFilmList.length > 0 &&
        <ListInstantSearch
          list={instantFilmList}
          header="Фильмы"
          imagePath="poster_path"
          namePath="title"
          originalNamePath="original_title"
          nameContent="film"
          onToggle={this.props.onToggle}
          onClick={this.props.getFilm}
        />
        }

        {instantTVShowList.length > 0 &&
        <ListInstantSearch
          list={instantTVShowList}
          header="Сериалы"
          imagePath="poster_path"
          namePath="name"
          originalNamePath="original_name"
          nameContent="tvShow"
          onToggle={this.props.onToggle}
          onClick={this.props.getTVShow}
        />
        }

        {instantPersonList.length > 0 &&
        <ListInstantSearch
          list={instantPersonList}
          header="Люди"
          imagePath="profile_path"
          namePath="name"
          nameContent="person"
          onToggle={this.props.onToggle}
          onClick={this.props.getPerson}
        />
        }

      </>
    )
  }
}

function mapStateToProps(state){
  return {
    instantFilmList: state.search.instantFilmList,
    instantTVShowList: state.search.instantTVShowList,
    instantPersonList: state.search.instantPersonList
  }
}

function mapDispatchToProps(dispatch){
  return {
    getFilm: filmId => dispatch(getFilm(filmId)),
    getTVShow: tvShowId => dispatch(getTVShow(tvShowId)),
    getPerson: personId => dispatch(getPerson(personId))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(InstantSearch);
