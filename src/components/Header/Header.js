import React from 'react';

import '../../styles/header.css';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';
import Search from './Search';

class Header extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      isOpen: false
    }
  }

  render(){
    return (
      <Navbar className="header" dark expand="md">
        <NavbarBrand href="/">
            FilmSearch
        </NavbarBrand>

        <Search/>

        <NavbarToggler onClick={() => this.setState({isOpen: !this.state.isOpen})}/>
        <Collapse isOpen={this.state.isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <NavLink href="https://bitbucket.org/davidov_sanya/">Bitbucket</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="https://www.themoviedb.org/">TheMovieDB</NavLink>
            </NavItem>
            {/*<UncontrolledDropdown nav inNavbar>*/}
            {/*  <DropdownToggle nav caret>*/}
            {/*    Options*/}
            {/*  </DropdownToggle>*/}
            {/*  <DropdownMenu right>*/}
            {/*    <DropdownItem>*/}
            {/*      Option 1*/}
            {/*    </DropdownItem>*/}
            {/*    <DropdownItem>*/}
            {/*      Option 2*/}
            {/*    </DropdownItem>*/}
            {/*    <DropdownItem divider/>*/}
            {/*    <DropdownItem>*/}
            {/*      Reset*/}
            {/*    </DropdownItem>*/}
            {/*  </DropdownMenu>*/}
            {/*</UncontrolledDropdown>*/}
          </Nav>
        </Collapse>
      </Navbar>
  )
  }
  }

  export default Header;
