import React from 'react';
import '../../../styles/list_info.css'
import getImage from '../../../api/getImage';
import { NavLink } from 'react-router-dom';

class ListInfo extends React.Component {

  onClick = (id) => () => {
    this.props.onClick(id);
  };

  render(){
    const {
      list,
      header,
      imagePath,
      namePath,
      originalNamePath,
      datePath,
      nameContent
    } = this.props;

    return (
      <div className="list-info">

        <h1 className="section-header">{`${header}`}</h1>

        {list.map((item, i) => {
          const key = `${nameContent}_${i}`;
          return (
            <NavLink
              to={`/${nameContent}/${item.id}`}
              key={key}
              className="list-info-item"
              onClick={this.onClick(item.id)}
            >

                <div className="num">
                  {i + 1}
                </div>

                <div>
                  <img width="40px" src={getImage(item[imagePath])} alt={key}/>
                </div>

                <div className="content">
                  <h3 className="film-name">
                    {item[namePath]}
                    {item[datePath] &&
                    <span className="date">{item[datePath].slice(0, 4)}</span>
                    }
                  </h3>
                  {item[originalNamePath] &&
                  <span className="original-name">{item[originalNamePath]}</span>
                  }
                </div>
                {item.vote_average != null &&
                <div className="rating">
                  {( item.vote_average === 0 ) ? ` —` : ` ${item.vote_average}`}
                </div>
                }

            </NavLink>
          )
        })
        }
      </div>
    );
  }
}

export default ListInfo;