import React from 'react';
import '../../../styles/list_short_info.css'
import { Link } from 'react-router-dom';

class ListShortInfo extends React.Component {

  onClick = (id) => () => {
    this.props.onClick(id)
  };

  render(){
    const {
      list,
      header,
      nameContent,
      number,
      namePath
    } = this.props;


    return (
      <div className="list_short_info">
        <h4>{`${header}`}</h4>
        <ul>

          {list.map((item, i) => {
            const key = `${nameContent}_${i}`;

            if (i < number) {
              return (
                <Link
                  to={`/${nameContent}/${item.id}`}
                  key={key}
                >
                  <li
                    onClick={this.onClick(item.id)}
                  >
                    {item[namePath]}
                  </li>
                </Link>
              )
            } else {
              return null
            }

          })}
          <li
            onClick={this.props.toggle}
          >
            ...
          </li>
        </ul>
      </div>
    );
  }
}

export default ListShortInfo;
