import React from 'react'
import {NavLink} from 'react-router-dom';
import '../../../styles/carousel.css'
import getImage from '../../../api/getImage';

class Carousel extends React.Component {

  constructor(id){
    super(id);
    this.state = {
      moveValue: 756,
      countItem: 7,
      widthItem: 108,
      position: 0,
    }
  }

  moveLeft = () => {
    const {widthItem, countItem} = this.state;
    let {position} = this.state;
    position += widthItem * countItem;
    position = Math.min(position, 0);
    this.setState({position});
  };

  moveRight = () => {
    const {widthItem, countItem} = this.state;
    const {list} = this.props;
    let {position} = this.state;
    position -= this.state.moveValue;
    position = Math.max(position, -widthItem * ( list.length - countItem ));
    this.setState({position});
  };

  getItem = (itemId) => () => {
    this.props.onClick(itemId);
  };

  render(){
    const {
      list,
      nameContent,
      nameProperty
    } = this.props;

    const {position} = this.state;

    return (
      <div className="my-carousel">

        <div
          className="arrow"
          onClick={this.moveLeft}
        >
          <div>
            <i className="fas fa-chevron-left"/>
          </div>
        </div>

        <ul style={{marginLeft: position}}>
          {list.map((item, i) => {
              const key = `${nameContent}_${i}`;

              return (
                <NavLink
                  to={`/${nameContent}/${item.id}`}
                  className="my-carousel-item"
                  key={key}
                  onClick={this.getItem(item.id)}
                >
                  <img width="58px" src={getImage(item.poster_path)} alt="Poster"/>
                  <div>
                    {item[nameProperty]}
                  </div>
                </NavLink>
              )
            }
          )}
        </ul>

        <div
          className="arrow"
          onClick={this.moveRight}
        >
          <div>
            <i className="fas fa-chevron-right"/>
          </div>
        </div>

      </div>
    );
  }
}

export default Carousel;
