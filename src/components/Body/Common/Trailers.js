import React from 'react'
import '../../../styles/trailers.css'

class Trailers extends React.Component {
  constructor(id){
    super(id);
    this.state = {
      numberTrailer: 0
    }
  }

  render(){
    const {numberTrailer} = this.state;
    const {videos} = this.props;
    return (
      <div className="trailers">
        <h1 className="section-header">
          Трейлеры:
        </h1>

        <ul>
          {videos.map((item, i) => {
            const key = `trailer_${i}`;
            return (
              <li
                key={key}
                onClick={() => this.setState({numberTrailer: i})}
              >
                {`Трейлер ${i + 1}`}
              </li>
            )
          })}
        </ul>
        <iframe
          title="video"
          width="560" height="315" src={`https://www.youtube.com/embed/${videos[numberTrailer].key}`}
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        />
      </div>
    );
  }
}

export default Trailers
