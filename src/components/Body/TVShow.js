import React from 'react';
import { connect } from 'react-redux'
import '../../styles/film.css';
import { getPerson, getTVShow } from '../../redux/actions/search';
import getImage from '../../api/getImage';
import Carousel from './Common/Carousel';
import Trailers from './Common/Trailers';
import ListShortInfo from './Common/ListShortInfo';
import ListInfo from './Common/ListInfo';
import { Helmet } from 'react-helmet';

class TVShow extends React.Component {

  constructor(id){
    super(id);
    this.state = {
      isShowActorList: false
    }
  }

  async componentDidMount(){
    try {
      await this.props.getTVShow(this.props.match.params.id);
    }
    catch (e) {
      this.props.history.push('/');
    }
  }


  getNumberSeasons = () => {
    const {number_of_seasons} = this.props.tvShow;

    if (number_of_seasons === 1) {
      return `(${number_of_seasons} сезон)`
    } else if (( ( number_of_seasons % 10 ) > 4 && ( number_of_seasons % 10 ) < 10 )
      || ( number_of_seasons % 10 ) === 0
      || ( ( number_of_seasons ) > 10 && ( number_of_seasons ) < 15 )) {
      return `(${number_of_seasons} сезонов)`
    } else return `(${number_of_seasons} сезона)`

  };

  getTVShow = (id) => {
    this.props.getTVShow(id);
    this.setState({
      isShowActorList: false
    });
  };

  showActorList = () => {
    const {isShowActorList} = this.state;
    this.setState({
      isShowActorList: !isShowActorList
    });
  };

  render(){
    const {
      tvShow,
      similar,
      recommendation,
      videos
    } = this.props;

    return (
      <>
        {tvShow &&
        <div className="film">

          <Helmet>
            <title>{`${tvShow.name}`}</title>
          </Helmet>

          <div className="main-info">

            <div>
              <img width="205px" src={getImage(tvShow.poster_path)} alt="Poster"/>
            </div>

            <div className="content">
              <div>
                <h1 className="film-name">
                  {tvShow.name}
                  <span className="original-name">
                  {`  сериал (${tvShow.first_air_date.slice(0, 4)}—${tvShow.last_air_date.slice(0, 4)})`}
                </span>
                </h1>
                <span className="original-name">{tvShow.original_name}</span>
              </div>

              <div className="general-info">
                <table className="table-info">
                  <tbody>
                  {tvShow.release_date !== '' &&
                  <tr>
                    <td className="type">год</td>
                    <td>{tvShow.first_air_date.slice(0, 4)} {this.getNumberSeasons()}</td>
                  </tr>
                  }

                  {tvShow.episode_run_time !== 0 && tvShow.episode_run_time &&
                  <tr>
                    <td className="type">длительность серии</td>
                    <td>{tvShow.episode_run_time} мин</td>
                  </tr>
                  }
                  {tvShow.vote_average !== '' && tvShow.vote_average !== 0 &&
                  <tr>
                    <td className="type">рейтинг IMDB</td>
                    <td>{tvShow.vote_average}</td>
                  </tr>
                  }

                  </tbody>
                </table>


                <ListShortInfo
                  list={tvShow.credits.cast}
                  header="В ролях:"
                  nameContent="person"
                  number={10}
                  namePath="name"
                  onClick={this.props.getPerson}
                  toggle={this.showActorList}
                />

              </div>

            </div>
          </div>

          {tvShow.overview &&
          <div className="description">
            <h1 className="section-header">
              Краткое содержание:
            </h1>
            <div>
              {tvShow.overview}
            </div>
          </div>
          }

          {this.state.isShowActorList &&
          <ListInfo
            list={tvShow.credits.cast}
            header="Список актёров:"
            imagePath="profile_path"
            namePath="name"
            nameContent="person"
            onClick={this.props.getPerson}
          />
          }

          {similar.length > 0 &&
          <>

            <h1 className="section-header">
              Похожие фильмы:
            </h1>

            <Carousel
              nameContent="tvShow"
              nameProperty="name"
              list={similar}
              onClick={this.getTVShow}
            />
          </>
          }

          {recommendation.length > 0 &&
          <>
            <h1 className="section-header">
              Рекомендации к фильму:
            </h1>

            <Carousel
              nameContent="tvShow"
              nameProperty="name"
              list={recommendation}
              onClick={this.getTVShow}
            />
          </>
          }

          {videos.length > 0 &&
          <Trailers
            videos={videos}
          />
          }

        </div>
        }
      </>
    )
  }
}

function mapStateToProps(state){
  return {
    tvShow: state.search.tvShow,
    similar: state.search.similar,
    recommendation: state.search.recommendation,
    videos: state.search.videos
  }
}

function mapDispatchToProps(dispatch){
  return {
    getPerson: personId => dispatch(getPerson(personId)),
    getTVShow: tvShowId => dispatch(getTVShow(tvShowId))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TVShow);
