import React from 'react';
import { connect } from 'react-redux'
import '../../styles/film.css';
import { getFilm, getPerson } from '../../redux/actions/search';
import getImage from '../../api/getImage';
import Carousel from './Common/Carousel';
import Trailers from './Common/Trailers';
import ListShortInfo from './Common/ListShortInfo';
import ListInfo from './Common/ListInfo';
import { Helmet } from 'react-helmet';

class Film extends React.Component {

  constructor(id){
    super(id);
    this.state = {
      isShowActorList: false
    }
  }

  async componentDidMount(){
     try {
       await this.props.getFilm(this.props.match.params.id);
     }
     catch (e) {
       this.props.history.push('/');
     }
  }

  getProductionCountries = () => {
    let str = '';
    this.props.film.production_countries.forEach((country) => {
      str += `${country.name}, `;
    });
    return str.slice(0, str.length - 2);
  };

  showActorList = () => {
    const {isShowActorList} = this.state;
    this.setState({
      isShowActorList: !isShowActorList
    });
  };

  getFilm = (id) => {
    this.props.getFilm(id);
    this.setState({
      isShowActorList: false
    });
  };

  render(){
    const {
      film,
      similar,
      recommendation,
      videos
    } = this.props;

    return (
      <>

        {film &&
        <div className="film">

          <Helmet>
            <title>{`${film.title}`}</title>
          </Helmet>

          <div className="main-info">
            <div>
              <img width="205px" src={getImage(film.poster_path)} alt="Poster"/>
            </div>

            <div className="content">
              <div>
                <h1 className="film-name">{film.title}</h1>
                <span className="original-name">{film.original_title}</span>
              </div>

              <div className="general-info">
                <table className="table-info">
                  <tbody>
                  {film.release_date !== '' &&
                  <tr>
                    <td className="type">год</td>
                    <td>{film.release_date.slice(0, 4)}</td>
                  </tr>
                  }
                  {film.production_countries.length > 0 &&
                  <tr>
                    <td className="type">страна</td>
                    <td>{this.getProductionCountries()}</td>
                  </tr>
                  }
                  {film.tagline !== '' &&
                  <tr>
                    <td className="type">слоган</td>
                    <td>{film.tagline}</td>
                  </tr>
                  }
                  {film.runtime !== 0 && film.runtime &&
                  <tr>
                    <td className="type">длительность</td>
                    <td>{film.runtime} мин</td>
                  </tr>
                  }
                  {film.vote_average !== '' && film.vote_average !== 0 &&
                  <tr>
                    <td className="type">рейтинг IMDB</td>
                    <td>{film.vote_average}</td>
                  </tr>
                  }

                  </tbody>
                </table>


                <ListShortInfo
                  list={film.credits.cast}
                  header="В ролях:"
                  nameContent="person"
                  number={10}
                  namePath="name"
                  onClick={this.props.getPerson}
                  toggle={this.showActorList}
                />

              </div>

            </div>
          </div>

          {film.overview &&
          <div className="description">
            <h1 className="section-header">
              Краткое содержание:
            </h1>
            <div>
              {film.overview}
            </div>
          </div>
          }

          {this.state.isShowActorList &&
          <ListInfo
            list={film.credits.cast}
            header="Список актёров:"
            imagePath="profile_path"
            namePath="name"
            nameContent="person"
            onClick={this.props.getPerson}
          />
          }


          {similar.length > 0 &&
          <>

            <h1 className="section-header">
              Похожие фильмы:
            </h1>

            <Carousel
              nameContent="film"
              nameProperty="title"
              list={similar}
              onClick={this.getFilm}
            />
          </>
          }

          {recommendation.length > 0 &&
          <>
            <h4>
              Рекомендации к фильму:
            </h4>

            <Carousel
              nameContent="film"
              nameProperty="title"
              list={recommendation}
              onClick={this.getFilm}
            />
          </>
          }

          {videos.length > 0 &&
          <Trailers
            videos={videos}
          />
          }

        </div>
        }
      </>
    )
  }
}


function mapStateToProps(state){
  return {
    film: state.search.film,
    similar: state.search.similar,
    recommendation: state.search.recommendation,
    videos: state.search.videos,
    error: state.search.error
  }
}

function mapDispatchToProps(dispatch){
  return {
    getPerson: personId => dispatch(getPerson(personId)),
    getFilm: filmId => dispatch(getFilm(filmId))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Film);
