import React from 'react';
import { connect } from 'react-redux'
import ListInfo from './Common/ListInfo';
import { getFilm, getPerson, getTVShow } from '../../redux/actions/search';
import { Helmet } from 'react-helmet';
import { Redirect } from 'react-router-dom';

class SearchResults extends React.Component {

  render(){
    const {
      searchFilmList,
      searchTVShowList,
      searchPersonList
    } = this.props;

    if (searchFilmList.length === 0 &&
      searchTVShowList.length === 0 &&
      searchPersonList.length === 0 ){
      return (
        <Redirect to='/' />
      );
    }

    return (
      <>

        <Helmet>
          <title>{`Результаты поиска`}</title>
        </Helmet>

        {searchFilmList.length !== 0 &&
        <ListInfo
          list={searchFilmList}
          header="Найденные фильмы:"
          imagePath="poster_path"
          namePath="title"
          originalNamePath="original_title"
          datePath="release_date"
          nameContent="film"
          onClick={this.props.getFilm}
        />
        }

        {searchTVShowList.length !== 0 &&
        <ListInfo
          list={searchTVShowList}
          header="Найденные сериалы:"
          imagePath="poster_path"
          namePath="name"
          originalNamePath="original_name"
          datePath="first_air_date"
          nameContent="tvShow"
          onClick={this.props.getTVShow}
        />
        }

        {searchPersonList.length !== 0 &&
        <ListInfo
          list={searchPersonList}
          header="Найденные актеры:"
          imagePath="profile_path"
          namePath="name"
          nameContent="person"
          onClick={this.props.getPerson}
        />
        }
      </>
    );
  }
}

function mapStateToProps(state){
  return {
    searchPersonList: state.search.searchPersonList,
    searchTVShowList: state.search.searchTVShowList,
    searchFilmList: state.search.searchFilmList
  }
}

function mapDispatchToProps(dispatch){
  return {
    getFilm: filmId => dispatch(getFilm(filmId)),
    getPerson: personId => dispatch(getPerson(personId)),
    getTVShow: tvShowId => dispatch(getTVShow(tvShowId))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchResults);
