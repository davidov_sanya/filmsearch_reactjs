import React from 'react';
import { connect } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router-dom'

import '../../styles/body.css'
import Film from './Film';
import Person from './Person';
import { getFilm, getPerson, getPopular, getTVShow } from '../../redux/actions/search';
import TVShow from './TVShow';
import SearchResults from './SearchResults';
import StartPage from './StartPage';

class Body extends React.Component {

  render(){
    return (
      <div className="body">

        <Switch>
          <Route path="/SearchResults" component={SearchResults}/>
          <Route path="/film/:id" component={Film}/>
          <Route path="/tvShow/:id" component={TVShow}/>
          <Route path="/person/:id" component={Person}/>
          <Route path="/" component={StartPage}/>

          <Redirect from="*" to="/" />
        </Switch>

      </div>
    )
  }
}



function mapDispatchToProps(dispatch){
  return {
    getPopular: () => dispatch(getPopular()),
    getFilm: filmId => dispatch(getFilm(filmId)),
    getPerson: personId => dispatch(getPerson(personId)),
    getTVShow: tvShowId => dispatch(getTVShow(tvShowId))
  }
}

export default connect(null, mapDispatchToProps)(Body);