import React from 'react';
import { connect } from 'react-redux';
import '../../styles/film.css';
import { getFilm, getPerson, getTVShow } from '../../redux/actions/search';
import getImage from '../../api/getImage';
import ListShortInfo from './Common/ListShortInfo';
import ListInfo from './Common/ListInfo';
import { Helmet } from 'react-helmet';

class Person extends React.Component {
  constructor(id){
    super(id);

    this.state = {
      isShowFilmList: false,
      isShowTVList: false
    }
  }

  async componentDidMount(){
    try {
      await this.props.getPerson(this.props.match.params.id);
    }
    catch (e) {
      this.props.history.push('/');
    }
  }


  showFilmList = () => {
    const {isShowFilmList} = this.state;
    this.setState({
      isShowFilmList: !isShowFilmList,
      isShowTVList: false
    });
  };

  showTVList = () => {
    const {isShowTVList} = this.state;
    this.setState({
      isShowTVList: !isShowTVList,
      isShowFilmList: false
    });
  };

  getFilm = (id) => {
    this.props.getFilm(id);
    this.setState({
      isShowFilmList: false
    });
  };

  getTVShow = (id) => {
    this.props.getTVShow(id);
    this.setState({
      isShowTVList: false
    });
  };

  render(){
    const {person} = this.props;

    return (
      <>
        {person &&
        <div className="film">

          <Helmet>
            <title>{`${person.name}`}</title>
          </Helmet>

          <div className="main-info">

            <div>
              <img width="205px" src={getImage(person.profile_path)} alt="Poster"/>
            </div>

            <div className="content">
              <div>
                <h1 className="film-name">{person.name}</h1>
              </div>

              <div className="general-info">
                <table className="table-info">
                  <tbody>
                  <tr>
                    <td className="type">дата рождения</td>
                    <td>{person.birthday}</td>
                  </tr>
                  {person.deathday &&
                  <tr>
                    <td className="type">дата смерти</td>
                    <td>{person.deathday}</td>
                  </tr>
                  }

                  <tr>
                    <td className="type">пол</td>
                    {person.gender === 1
                      ? <td>ж</td>
                      : <td>м</td>
                    }

                  </tr>

                  <tr>
                    <td className="type">место рождения</td>
                    <td>{person.place_of_birth}</td>
                  </tr>

                  </tbody>
                </table>

                <div className="actor-list-info">
                  {person.movie_credits.cast.length > 0 &&
                  <ListShortInfo
                    list={person.movie_credits.cast}
                    header="Фильмы:"
                    nameContent="film"
                    number={5}
                    namePath="title"
                    onClick={this.props.getFilm}
                    toggle={this.showFilmList}
                  />
                  }

                  {person.tv_credits.cast.length > 0 &&
                  <ListShortInfo
                    list={person.tv_credits.cast}
                    header="Сериалы:"
                    nameContent="tvShow"
                    number={5}
                    namePath="name"
                    onClick={this.props.getTVShow}
                    toggle={this.showTVList}
                  />
                  }
                </div>
              </div>

            </div>
          </div>

          {person.biography &&
          <div className="description">
            <h1 className="section-header">
              Биография:
            </h1>
            <div>
              {person.biography}
            </div>
          </div>
          }

          {this.state.isShowFilmList &&
          <ListInfo
            list={person.movie_credits.cast}
            header="Фильмы:"
            imagePath="poster_path"
            namePath="title"
            originalNamePath="original_title"
            datePath="release_date"
            nameContent="film"
            onClick={this.getFilm}
          />
          }

          {this.state.isShowTVList &&
          <ListInfo
            list={person.tv_credits.cast}
            header="Сериалы:"
            imagePath="poster_path"
            namePath="name"
            originalNamePath="original_name"
            datePath="first_air_date"
            nameContent="tvShow"
            onClick={this.getTVShow}
          />
          }

        </div>
        }
      </>
    )
  }
}

function mapStateToProps(state){
  return {
    person: state.search.person
  }
}

function mapDispatchToProps(dispatch){
  return {
    getPerson: personId => dispatch(getPerson(personId)),
    getFilm: filmId => dispatch(getFilm(filmId)),
    getTVShow: tvShowId => dispatch(getTVShow(tvShowId))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Person);
