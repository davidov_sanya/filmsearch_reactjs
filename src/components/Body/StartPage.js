import React from 'react';
import {connect} from 'react-redux'
import ListInfo from './Common/ListInfo';
import { getFilm } from '../../redux/actions/search';

class StartPage extends React.Component {

  render(){
    const {popular} = this.props;
    return (
      <>

        {popular &&
        <ListInfo
          list={popular}
          header="Популярное сегодня:"
          imagePath="poster_path"
          namePath="title"
          originalNamePath="original_title"
          datePath="release_date"
          nameContent="film"
          onClick={this.props.getFilm}
        />
        }

      </>
    );
  }
}

function mapStateToProps(state){
  return {
    popular: state.search.popular
  }
}

function mapDispatchToProps(dispatch){
  return {
    getFilm: filmId => dispatch(getFilm(filmId))
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(StartPage);
