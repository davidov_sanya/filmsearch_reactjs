import React from 'react';

import '../styles/go-up.css'

class GoUp extends React.Component {
  constructor(id){
    super(id);

    this.state = {
      showGoUp: 'none',
      showGoDown: 'none',
      topGoDown: 56,
      savePosition: 0
    };

    window.addEventListener('scroll', this.scroll);
  }

  scroll = () => {
    if (document.documentElement.scrollTop > document.documentElement.clientHeight) {
      this.setState({
        showGoUp: 'inline',
        showGoDown: 'none'
      });
    } else {
      this.setState({showGoUp: 'none'});
    }

    if (document.documentElement.scrollTop > 56) {
      this.setState({
        topGoDown: 0
      });
    } else {
      this.setState({
        topGoDown: 56 - document.documentElement.scrollTop
      });
    }

  };

  toGoUp = () => {
    this.setState({
      savePosition: document.documentElement.scrollTop,
      showGoDown: 'inline'
    });
    document.documentElement.scrollTo(0, 0);
  };

  toGoDown = () => {
    document.documentElement.scrollTo(0, this.state.savePosition);
    this.setState({
      showGoDown: 'none',
    });
  };

  render(){
    return (
      <>
        <div style={{display: this.state.showGoUp}}>
          <div
            className="go-up"
            onClick={this.toGoUp}
          >
            <i className="far fa-caret-square-up button-up"/>
            <div className="background up"/>
            <p>наверх</p>
          </div>
          <i className="far fa-caret-square-up button-up"/>
        </div>


        <div style={{display: this.state.showGoDown}}>
          <div
            className="go-down"
            style={{top: this.state.topGoDown}}
            onClick={this.toGoDown}
          >
            <i className="far fa-caret-square-down button-down"
               style={{top: this.state.topGoDown + 35}}
            />
            <div className="background down"/>
          </div>
          <i className="far fa-caret-square-down button-down"
             style={{top: this.state.topGoDown + 35}}
          />
        </div>
      </>
    );
  }
}

export default GoUp;
