import {
  ERROR,
  GET_FILM_SUCCESS,
  GET_INSTANT_FILM_LIST_SUCCESS,
  GET_INSTANT_PERSON_LIST_SUCCESS,
  GET_INSTANT_TV_SHOW_LIST_SUCCESS,
  GET_PERSON_SUCCESS,
  GET_POPULAR_SUCCESS,
  GET_RECOMMENDATION_SUCCESS,
  GET_SIMILAR_SUCCESS,
  GET_TV_SHOW_SUCCESS,
  GET_VIDEOS_SUCCESS,
  ON_TOGGLE_RESULTS
} from './actionTypes';
import axios from 'axios';

export function getFilmList(value){

  return async dispatch => {
    try {
      if (value === '') {
        dispatch(getFilmListSuccess([]));
      } else {
        const response = await axios.get(`https://api.themoviedb.org/3/search/movie?api_key=8ff425c4728d208379f4ded9770872cc&query=${value}&language=ru`);
        const instantFilmList = response.data.results;
        dispatch(getFilmListSuccess(instantFilmList));
      }

    } catch (e) {
      throw e;
    }
  }
}

export function getTVShowList(value){

  return async dispatch => {
    try {
      if (value === '') {
        dispatch(getTVShowListSuccess([]));
      } else {
        const response = await axios.get(`https://api.themoviedb.org/3/search/tv?api_key=8ff425c4728d208379f4ded9770872cc&query=${value}&language=ru`);
        const instantTVShowList = response.data.results;
        dispatch(getTVShowListSuccess(instantTVShowList));
      }

    } catch (e) {
      throw e;
    }
  }
}

export function getPersonList(value){

  return async dispatch => {
    try {
      if (value === '') {
        dispatch(getPeopleListSuccess([]));
      } else {
        const response = await axios.get(`https://api.themoviedb.org/3/search/person?api_key=8ff425c4728d208379f4ded9770872cc&query=${value}&language=ru`);
        const instantPersonList = response.data.results;
        dispatch(getPeopleListSuccess(instantPersonList));
      }

    } catch (e) {
      throw e;
    }
  }
}

export function getPopular() {

  return async dispatch => {
    try {
      const response = await axios.get(`https://api.themoviedb.org/3/movie/popular?api_key=8ff425c4728d208379f4ded9770872cc&append_to_response=movie_credits&language=ru&page=1`);
      const popular = response.data.results;
      dispatch(getPopularSuccess(popular));
    }
    catch (e) {
      throw e;
    }
  }
}

export function getPerson(value){

  return async dispatch => {
    try {
      if (value === '') {
        dispatch(getPeopleSuccess([]));
      } else {
        const response = await axios.get(`https://api.themoviedb.org/3/person/${value}?api_key=8ff425c4728d208379f4ded9770872cc&append_to_response=movie_credits,tv_credits&language=ru`);
        const person = response.data;
        dispatch(getPeopleSuccess(person));
        return person;
      }

    } catch (e) {
      throw e;
    }
  }
}


export function getFilm(value){
  return async dispatch => {
    try {
      if (value === '') {
        dispatch(getFilmSuccess([]));
      } else {
        const responseFilm = await axios.get(`https://api.themoviedb.org/3/movie/${value}?api_key=8ff425c4728d208379f4ded9770872cc&append_to_response=credits&language=ru`);
        const film = responseFilm.data;
        dispatch(getFilmSuccess(film));
        dispatch(getSimilar('movie', film.id));
        dispatch(getRecommendations('movie', film.id));
        dispatch(getVideos('movie', film.id));
        return film;
      }


    } catch (e) {
      throw e;
    }
  }
}



export function getTVShow(value){
  return async dispatch => {
    try {
      if (value === '') {
        dispatch(getTVShowSuccess([]));
      } else {
        //dispatch(getListStart());
        const response = await axios.get(`https://api.themoviedb.org/3/tv/${value}?api_key=8ff425c4728d208379f4ded9770872cc&append_to_response=credits&language=ru`);
        const tvShow = response.data;
        dispatch(getTVShowSuccess(tvShow));
        dispatch(getSimilar('tv', tvShow.id));
        dispatch(getRecommendations('tv', tvShow.id));
        dispatch(getVideos('tv', tvShow.id));
        return tvShow;
      }

    } catch (e) {
      throw e;
    }
  }
}

function getSimilar(type, id) {
  return async dispatch => {
    try {
      const responseSimilar = await axios.get(`https://api.themoviedb.org/3/${type}/${id}/similar?api_key=8ff425c4728d208379f4ded9770872cc&language=ru&page=1`);
      const similar = responseSimilar.data.results;
      dispatch(getSimilarSuccess(similar));
    } catch (e) {
      throw e;
    }
  }
}

function getRecommendations(type, id) {
  return async dispatch => {
    try {
      const responseRecommendation = await axios.get(`https://api.themoviedb.org/3/${type}/${id}/recommendations?api_key=8ff425c4728d208379f4ded9770872cc&language=ru&page=1`);
      const recommendation = responseRecommendation.data.results;
      dispatch(getRecommendationSuccess(recommendation));
    } catch (e) {
      throw e;
    }
  }
}

function getVideos(type, id) {
  return async dispatch => {
    try {
      const response = await axios.get(`https://api.themoviedb.org/3/${type}/${id}/videos?api_key=8ff425c4728d208379f4ded9770872cc&language=ru`);
      const videos = response.data.results;
      dispatch(getVideosSuccess(videos));
    } catch (e) {
      throw e;
    }
  }
}


export function getFilmListSuccess(instantFilmList){
  return {
    type: GET_INSTANT_FILM_LIST_SUCCESS,
    instantFilmList
  }
}

export function getTVShowListSuccess(instantTVShowList){
  return {
    type: GET_INSTANT_TV_SHOW_LIST_SUCCESS,
    instantTVShowList
  }
}


export function getPeopleListSuccess(instantPersonList){
  return {
    type: GET_INSTANT_PERSON_LIST_SUCCESS,
    instantPersonList
  }
}

export function getPopularSuccess(popular){
  return {
    type: GET_POPULAR_SUCCESS,
    popular
  }
}

export function getFilmSuccess(film){
  return {
    type: GET_FILM_SUCCESS,
    film
  }
}

export function getSimilarSuccess(similar){
  return {
    type: GET_SIMILAR_SUCCESS,
    similar
  }
}

export function getRecommendationSuccess(recommendation){
  return {
    type: GET_RECOMMENDATION_SUCCESS,
    recommendation
  }
}

export function getVideosSuccess(videos){
  return {
    type: GET_VIDEOS_SUCCESS,
    videos
  }
}

export function getTVShowSuccess(tvShow){
  return {
    type: GET_TV_SHOW_SUCCESS,
    tvShow
  }
}

export function getPeopleSuccess(person){
  return {
    type: GET_PERSON_SUCCESS,
    person
  }
}

export function onSearchResults(){
  return {
    type: ON_TOGGLE_RESULTS
  }
}

export function onError(){
  return {
    type: ERROR
  }
}
