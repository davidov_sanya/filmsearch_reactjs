import {combineReducers} from 'redux';
import instantSearch from './instantSearch';
import search from './search';

export default combineReducers({
  search,
  instantSearch
});