import {
  ERROR,
  //GET_FILM_LIST_SUCCESS,
  GET_FILM_SUCCESS,
  GET_INSTANT_FILM_LIST_SUCCESS,
  GET_INSTANT_PERSON_LIST_SUCCESS,
  GET_INSTANT_TV_SHOW_LIST_SUCCESS,
  //GET_PERSON_LIST_SUCCESS,
  GET_PERSON_SUCCESS,
  GET_POPULAR_SUCCESS,
  GET_RECOMMENDATION_SUCCESS,
  GET_SIMILAR_SUCCESS,
  GET_TV_SHOW_SUCCESS,
  GET_VIDEOS_SUCCESS,
  //ON_SHOW_FILM_LIST,
  //ON_SHOW_PERSON_LIST,
  ON_TOGGLE_RESULTS
} from '../actions/actionTypes';

const initialState = {
  popular: [],

  instantFilmList: [],
  instantTVShowList: [],
  instantPersonList: [],

  searchFilmList: [],
  searchTVShowList: [],
  searchPersonList: [],

  film: null,
  similar: [],
  recommendation: [],
  videos: [],
  tvShow: null,
  person: null,

  error: false
};

export default function search(state = initialState, action){
  switch ( action.type ) {

    case GET_INSTANT_FILM_LIST_SUCCESS:
      return {
        ...state,
        instantFilmList: action.instantFilmList
      };

    case GET_INSTANT_TV_SHOW_LIST_SUCCESS:
      return {
        ...state,
        instantTVShowList: action.instantTVShowList
      };

    case GET_INSTANT_PERSON_LIST_SUCCESS:
      return {
        ...state,
        instantPersonList: action.instantPersonList
      };

    case GET_FILM_SUCCESS:
      return {
        ...state,
        error: false,
        similar: [],
        recommendation: [],
        searchFilmList: [],
        searchTVShowList: [],
        searchPersonList: [],
        film: action.film
      };



    case GET_TV_SHOW_SUCCESS:
      return {
        ...state,
        error: false,
        similar: [],
        recommendation: [],
        searchFilmList: [],
        searchTVShowList: [],
        searchPersonList: [],
        tvShow: action.tvShow
      };

    case GET_SIMILAR_SUCCESS:
      return {
        ...state,
        similar: action.similar
      };

    case GET_RECOMMENDATION_SUCCESS:
      return {
        ...state,
        recommendation: action.recommendation
      };

    case GET_VIDEOS_SUCCESS:
      return {
        ...state,
        videos: action.videos
      };

    case GET_PERSON_SUCCESS:
      return {
        ...state,
        error: false,
        similar: [],
        recommendation: [],
        searchFilmList: [],
        searchTVShowList: [],
        searchPersonList: [],
        person: action.person
      };

    case GET_POPULAR_SUCCESS:
      return {
        ...state,
        popular: action.popular
    };

    case ON_TOGGLE_RESULTS:
      return {
        ...state,
        searchFilmList: state.instantFilmList,
        searchTVShowList: state.instantTVShowList,
        searchPersonList: state.instantPersonList,
      };

    case ERROR:
      return {
        ...state,
        error: true
      };

    default:
      return state;
  }
}